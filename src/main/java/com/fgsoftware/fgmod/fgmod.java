package com.fgsoftware.fgmod;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.LoaderState;
import net.minecraftforge.fml.common.Mod;

import org.apache.logging.log4j.LogManager;

import javax.annotation.Nonnull;

import java.util.logging.Logger;

@Mod(modid = fgmod.MOD_ID, name = fgmod.MOD_NAME, version = fgmod.MC_VERSION)
public class fgmod{
    public static final @Nonnull String MOD_ID = "fgmod";
    public static final @Nonnull String MOD_NAME = "fgmod";
    public static final @Nonnull String MC_VERSION = "1.8.9";
    public static final Logger log = (Logger) LogManager.getLogger("fgmod");

    public void init() {
        if(!Loader.instance().isInState(LoaderState.INITIALIZATION)) {
            fgmod.log.info(
                    "Proxy.init has to be called during Initialisation.");
        }
    }
}
